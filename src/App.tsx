import React, { useState } from 'react';
import './App.scss';
import Router from './pages/router';
import Login from './pages/login';


function App() {

  const [token, setToken] = useState()

  return (
    <div>
        <Router />
    </div>
  );
}

export default App;
