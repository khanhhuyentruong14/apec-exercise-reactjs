import React from 'react'
import AppLayout from '..'
import Content from './Content'

const Test = () => {
  return (
      <AppLayout element={<Content />} />
  )
}

export default Test
