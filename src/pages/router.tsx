import React from 'react'
import { Route, Routes } from 'react-router-dom'
import Login from './login'
import ListStudent from './student/list'
import Test from './test'

const Router = () => {
  return (
    <div>
      <Routes>
          <Route path="/" index element={<Login />}></Route>
          <Route path="/list" element={<ListStudent />}></Route>
          <Route path="/test" element={<Test />}></Route>
        </Routes>
    </div>
  )
}

export default Router
