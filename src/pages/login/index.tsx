import React, { useState } from 'react'
import './style.scss'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'

const Login = () => {

  const init = {
    username: '',
    password: ''
  }

  const [account, setAccount] = useState(init)
  const [errMsg, setErrMsg] = useState<String>("")
  const naviage = useNavigate()

  const handleChangeAccount = (e: any) => {
    setAccount({ ...account, [e.target.name]: e.target.value })
    console.log(account)
  }

  const handleSubmit = (e: any) => {
    e.preventDefault();
  }

  const login = () => {
    if (account.username === '1' && account.password === '1')
      naviage('/list')
    else setErrMsg('Wrong login information!')
  }

  return (
    <div className='login d-flex justify-content-center align-items-center flex-wrap'>
      <div className="col-lg-7 col-md-6 col-sm-12 login-img">
        <img src="https://assets.vpm.org/c6/f2/f45b430b41b29a09ce6f91eaa999/internship-square.png" alt="" />
      </div>
      <div className="col-lg-5 col-md-6 col-sm-12">
        <form onSubmit={handleSubmit} className='login-form'>
          <div className="mb-4 text-center login-form-item">
            <h1>Login</h1>
          </div>
          {
            errMsg ? <p className='text-danger text-center'>* {errMsg}</p> : ''
          }
          <div className="mb-4 login-form-item">
            <input type="text" id="username" className="form-control" name='username'
              placeholder="Enter username" onChange={handleChangeAccount} />
          </div>
          <div className="mb-4 login-form-item">
            <input type="password" id="password" className="form-control" name='password'
              placeholder="Enter password" onChange={handleChangeAccount} />
          </div>
          <div className="text-center login-form-item">
            <button type="submit" className="btn btn-primary"
              onClick={login}>
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Login
