import React, { useEffect, useState } from 'react'
import './style.scss'
import { studentApi } from '../../../services/studentApi'


const ListStudent = () => {

  const [listSt, setListSt] = useState<any>([])

  const rowOfPage = 8

  useEffect(() => {
    studentApi.getAll().then(response => {
      if (response.data?.length > 0) {
        setListSt(response.data);
      }
    })

  }, [])

  // Cắt mảng
  function Paginator(items: any, page: any, per_page: any) {
    var page = page || 1,
      per_page = per_page || 10,
      offset = (page - 1) * per_page,
      paginatedStudents = items.slice(offset).slice(0, per_page)
    return paginatedStudents
  }

  const total_pages = Math.ceil(listSt.length / rowOfPage);

  // Đánh số trang
  let items = [];
  let resultPage:any = []
  for (let number = 1; number <= total_pages; number++) {
    items.push(number);
    resultPage[number] = Paginator(listSt, number, rowOfPage)
  }
  console.log('page1', resultPage[1])
  const [pageData, setPageData] = useState<any>(resultPage[1])
  useEffect(() => {
      setPageData(resultPage[1])
  }, [listSt])
  console.log('pageData', pageData)
  
  return (
    <div className='list-table'>
      <div className="list-table-container">
        <div className="list-table-content mt-5">
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Class</th>
                <th scope="col">Birth</th>
                <th scope="col">Address</th>
                <th scope="col">Phone</th>
                <th scope="col">Email</th>
              </tr>
            </thead>
            <tbody>
              {pageData?.map((st: any, index: any) =>
                <tr key={index}>
                  <th scope="row">{listSt.indexOf(st) + 1}</th>
                  <td>{st.mssv}</td>
                  <td>{st.name}</td>
                  <td>{st.class}</td>
                  <td>{new Date(st.dateOfBirth).toLocaleDateString('en-US')}</td>
                  <td>{st.address}</td>
                  <td>{st.phone}</td>
                  <td>{st.email}</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <div className='page-number d-flex mt-4 mb-5 justify-content-center align-items-center flex-wrap'>
          <i className="fa-solid fa-angle-left me-2"></i>
          {items.map((item, index) =>
            <p key={index}
              onClick={() => {
                setPageData(resultPage[index + 1])
              }}>{item}</p>
          )}
          <i className="fa-solid fa-angle-right ms-2"></i>
        </div>
      </div>
    </div>
  )
}

export default ListStudent
