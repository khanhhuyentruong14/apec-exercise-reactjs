import React from 'react'
import AppLayout from '../..'
import Table from './Table'

const ListStudent = () => {
  return (
    <AppLayout element={<Table />} />
  )
}

export default ListStudent
