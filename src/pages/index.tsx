import React from 'react'
import Header from '../component/header'
import Sidebar from '../component/sidebar'

const AppLayout = (props: any) => {
  return (
    <div className='app d-flex'>
      <div className="col-lg-2 col-12 app-sidebar">
        <Sidebar />
      </div>
      <div className="col-lg-10 col-12 app-content">
        <div className="app-content-header d-flex">
          <Header />
        </div>
        {props.element}
      </div>
    </div>
  )
}

export default AppLayout
