import React, { useState } from 'react'
import './style.scss'
import Sidebar from '../sidebar'
import { useNavigate } from 'react-router-dom'

const Header = () => {

  const [hidden, setHidden] = useState(true)
  const naviage = useNavigate()

  return (
    <div className='header d-flex justify-content-between align-items-center'>
      <div className="header-sidebar-open"
        onClick={() => {
          setHidden(!hidden)
        }}>
        <i className="fa-solid fa-bars"></i>
      </div>
      <div className="header-logo">
        <img src="https://racevietnam.com/Thumbnail/ExtraLarge/Upload/20220525/d2f65be0277c4ffab96f32c1170d6248.png" alt="" />
        <span className='ms-2'>APEC</span>
      </div>
      <div className="header-logout text-center"
      onClick={() => {
        naviage('/')
      }}>
        <i className="fa-solid fa-right-to-bracket"></i>
      </div>
      <div className={`header-sidebar ${hidden ? 'hide' : ''}`}>
        <Sidebar />
        <div className="header-sidebar-close"
          onClick={() => {
            setHidden(!hidden)
          }}>
          <i className="fa-solid fa-xmark"></i>
        </div>
      </div>
    </div>
  )
}

export default Header

