import React from 'react'
import './style.scss'
import { useNavigate } from 'react-router-dom'


const Sidebar = () => {
  const navigate = useNavigate()
  return (
    <div className='sidebar'>
      <div className="sidebar-header d-flex align-items-center px-3">
        <div className="sidebar-header-img">
          <img src="https://png.pngtree.com/png-clipart/20190924/original/pngtree-vector-user-young-boy-avatar-icon-png-image_4827810.jpg" alt="" />
        </div>
        <div className="sidebar-header-user ps-2">
          <p>Admin User</p>
          <p>System Manager</p>
        </div>
      </div>
      <div className="sidebar-menu mt-4 px-4">
        <p>Student Management System</p>
        <ul>
          <li onClick={() => navigate('/list')}>
            <i className="fa-solid fa-list me-2"></i>
            Student List
          </li>
          <li onClick={() => navigate('/test')}>
            <i className="fa-solid fa-gear me-2"></i>
            Test
          </li>
        </ul>
      </div>
    </div>
  )
}

export default Sidebar
