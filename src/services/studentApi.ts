import axios from "axios";

const axiosClient = axios.create({
  baseURL: 'https://6495c9ceb08e17c91792add0.mockapi.io'
})

const endpoint = '/students'

export const studentApi = {
  getAll() {
    return axiosClient.get(endpoint)
  },

  add(data:any) {
    return axiosClient.post(endpoint, data)
  }
}